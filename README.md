# WGMODES

I (Pawel) am **NOT** the author of this code, nor is it developed here.
This is simply an upload of the contents of
[http://photonics.umd.edu/wp-content/uploads/software/wgmodes/modesolver-2011-04-22.zip](http://photonics.umd.edu/wp-content/uploads/software/wgmodes/modesolver-2011-04-22.zip),
also found at
[https://www.mathworks.com/matlabcentral/fileexchange/12734-waveguide-mode-solver](https://www.mathworks.com/matlabcentral/fileexchange/12734-waveguide-mode-solver).
Consult the original authors for any questions, comments, issues, or any other reason.

The method implemented in this code is published in:

A. B. Fallahkhair, K. S. Li and T. E. Murphy, 
["Vector Finite Difference Modesolver for Anisotropic Dielectric Waveguides"](http://photonics.umd.edu/pubs/ja-20/)
, *J. Lightwave Technol.* **26**(11), 1423-1431, (2008).
